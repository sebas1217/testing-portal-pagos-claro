import { should } from "chai"

class PortalPagosClaro_PO {
    visitPortal() {
        beforeEach(() => {
            cy.visit("https://portalpagos.claro.com.co/index.php?view=vistas/personal/claro/newclaro/confirmacion.php&id_objeto=10000#no-back-button")
        })
    }

    FormularioCompraPaquetes(numeroCelular,seleccionPack, tiempo){
        let time = tiempo
        cy.get('#select')
            .should("be.visible")
            .click({force: true})
        cy.wait(time)
        cy.get(':nth-child(2) > .contenido-opcion')
            .click({force: true})
        cy.wait(time)
        cy.get('#NumeroCelular')
            .should("be.visible")
            .type(numeroCelular)
            .blur()
        cy.wait(time)
        cy.get('#TIPO_COMPRA')
            .should("be.enabled")
            .select(seleccionPack, {force:true})
        cy.wait(time)
        cy.get('.right > .glyphicon')
            .click()
        cy.wait(time) 
        cy.get('#button_50129')
            .click()
        cy.wait(time)    
        cy.get('#mySubmit_')
            .should("be.enabled")
            .should('contain','Continuar',)
            .click()
    }

    SeleccionMetodPago(numeroCelular,tiempo){
        let time = tiempo
        
        cy.get(':nth-child(7) > .col-md-8')
            .should('contain',numeroCelular)
        cy.wait(time)
        cy.get('#select')
            .click({force:true})
        cy.get(':nth-child(2) > .contenido-opcion')
            .click({force: true})
        cy.pause()//Realizar el captcha
        cy.get('#mySubmit_')
            .should("be.enabled")
            .should("contain","Confirmar")
            .click()
    }

    FormularioPSE(seleccionBanco, nombreTitular,numeroIdentificacion, email, tiempo){
        let time = tiempo
        cy.get('#BANCO')
            .select(seleccionBanco)
        cy.wait(time)
        cy.get('#TITULAR')
            .should("be.visible")
            .should("be.enabled")
            .type(nombreTitular)
        cy.wait(time)
        cy.get('#TIPO_CLIENTE')
            .should('contain','Persona Natural')
        cy.wait(time)
        cy.get('#TIPO_CLIENTE')
            .should("be.visible")
        cy.wait(time)
        cy.get('#NUMERO_DOCUMENTO')
            .should('be.enabled')
            .type(numeroIdentificacion)
        cy.wait(time)
        cy.get('#TELEFONO')
            .should('be.visible')
            .should('be.enabled')
        cy.wait(time)
        cy.get('#EMAIL')
            .should('be.visible')
            .should('be.enabled')
            .type(email)
        cy.wait(time)
        cy.get('#mySubmit_')
            .click()
        
    }

}//Final
export default PortalPagosClaro_PO