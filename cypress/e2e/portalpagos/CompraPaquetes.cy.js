import PortalPagosClaro_PO from '../../support/pageObjectsModels/portalPagosClaro/PortalPagosClaro_POO.cy'
///<reference types = "Cypress" />
//Para que funcionen los comandos



describe("Portal pagos Claro - Compra de paquetes", () => {

    const master = new PortalPagosClaro_PO
    master.visitPortal()
    /*Los datos se pueden cambiar por otros para validar los posibles escenearios de esta forma se evita codigo spaguetti*/
    it('Formulario compra paquetes', () => {
        master.FormularioCompraPaquetes("3202750805", 0, 3000)
        master.SeleccionMetodPago("3202750805", 3000)
        master.FormularioPSE(3,"Sebastian Carvajal",1113688998,"sebas.carva97@gmail.com", 3000)
    })
})